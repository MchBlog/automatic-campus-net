﻿namespace AutoInternet.Utils;

public class ColorConsole
{

    public static void log(string message)
        {
            Console.WriteLine(message);
        }

        public static void log(string message, ConsoleColor ForegroundColor)
        {
            Console.ForegroundColor = ForegroundColor;
            Console.WriteLine(message);
        }

        public static void logAsync(string message, ConsoleColor ForegroundColor, bool isInline)
        {
            Console.ForegroundColor = ForegroundColor;
            if (isInline)
            {
                Console.Write(message);
            }
            else
            {
                Console.WriteLine(message);
            }
        }

    
        
}