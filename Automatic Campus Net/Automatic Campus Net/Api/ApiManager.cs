﻿using System.Reflection;
using AutoInternet.Utils;
using Automatic_Campus_Net.Model;

namespace Automatic_Campus_Net.Api;

public class ApiManager
{
    private static String loginApi = "/eportal/portal/login";

    private static String logoutApi = "/eportal/portal/logout";

    public static string LoginApi => loginApi;

    public static string LogoutApi => logoutApi;


    public static String GeneralRequestResource(String baseResource,RequestModel requestModel)
    {
        String resource = $"{baseResource}?";
        Type type = requestModel.GetType();
        PropertyInfo[] propertyInfos = type.GetProperties();
        foreach (PropertyInfo propertyInfo in propertyInfos)
        {
            bool hasTargetAttribute = propertyInfo.GetCustomAttributes().ToList().Any(i => i is NeedToAddRequestResourceAttribute);
            if (hasTargetAttribute)
            {
                resource += $"{propertyInfo.Name}={propertyInfo.GetValue(requestModel)}&";
            }
        }
        resource = resource.Substring(0, resource.Length-1);
        return resource;
    }
}