﻿using System.Net.NetworkInformation;
using AutoInternet.Utils;
using Automatic_Campus_Net.Api;
using Automatic_Campus_Net.Model;
using Figgle;

ColorConsole.log(FiggleFonts.Ogre.Render("Automatic Campus Net"), ConsoleColor.Blue);
ColorConsole.log(
    "================================================ v 1.0 ================================================",
    ConsoleColor.Cyan);
ColorConsole.log(
    "============================================= author: Hui =============================================",
    ConsoleColor.Cyan);
ColorConsole.log(
    "========================================== Thanks for using it ========================================",
    ConsoleColor.Cyan);
// 校验配置文件
String configFilePath = Path.Combine(Directory.GetCurrentDirectory(), "config.json");
RequestModel? requestModel = null;
if (!File.Exists(configFilePath))
{
    string requestJsonData = JsonUtil.SerializeObject(new RequestModel());
    await JsonUtil.WriteJsonFile(configFilePath, requestJsonData);
    ColorConsole.log("首次使用请先完成配置文件，以免发生不必要的问题", ConsoleColor.Red);
    ColorConsole.log(
        "=====================================================================================================",
        ConsoleColor.Yellow);
    return;
}
else
{
    try
    {
        requestModel = JsonUtil.DeserializeObject<RequestModel>(File.ReadAllText(configFilePath));
        HttpUtils.SetRequestModel(requestModel);
    }
    catch (Exception e)
    {
        Console.WriteLine("配置文件发生错误，请先修改配置文件，必要的时候可以将配置文件删除，我们将为你重新生成配置文件");
    }
}

ColorConsole.log("当前时间: " + DateTime.Now,ConsoleColor.Blue);
// 间隔时间
int sleepTime = 2;
Thread thread = new Thread(async () =>
{
    Ping ping = new Ping();
    while (true)
    {
        try
        {
            PingReply pingReply = ping.Send(requestModel.TargetIp);
            if (pingReply.Status != IPStatus.Success)
            {
                GetInternet(requestModel);
            }
            else if (pingReply.Status == IPStatus.Success)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.Write('*');
            }
        }
        catch (Exception e)
        {
            GetInternet(requestModel);
        }

        Thread.Sleep(sleepTime * 1000);
    }
});

thread.Start();

static void GetInternet(RequestModel requestModel)
{
    ColorConsole.log("Internet connection is bad",ConsoleColor.Red);
    ColorConsole.log("Recertifying",ConsoleColor.Yellow);
    ResultModel? resultModel =
        HttpUtils.Get<ResultModel>(ApiManager.GeneralRequestResource(ApiManager.LoginApi, requestModel));
    if (resultModel != null)
    {
        switch (resultModel.result)
        {
            case 0:
                ColorConsole.log("登录已过期，正在重新登录",ConsoleColor.Blue);
                ResultModel? model = HttpUtils.Get<ResultModel>(ApiManager.LogoutApi);
                if (model.result == 1)
                {
                    ColorConsole.log("Reconnection succeeded",ConsoleColor.Green);
                    ColorConsole.log(resultModel.msg, ConsoleColor.Green);
                    ColorConsole.log("️修复时间: " + DateTime.Now,ConsoleColor.Blue);
                }
                else
                {
                    ColorConsole.log($"连接失败 {resultModel.msg} 重新连接", ConsoleColor.Red);
                    GetInternet(requestModel);
                    ColorConsole.log("️修复时间: " + DateTime.Now,ConsoleColor.Blue);
                }

                break;
            case 1:
                ColorConsole.log("Reconnection succeeded",ConsoleColor.Green);
                ColorConsole.log(resultModel.msg, ConsoleColor.Green);
                ColorConsole.log("️修复时间: " + DateTime.Now,ConsoleColor.Blue);
                break;
            default:
                ColorConsole.log($"连接失败 {resultModel.msg} 重新连接", ConsoleColor.Red);
                GetInternet(requestModel);
                ColorConsole.log("️修复时间: " + DateTime.Now ,ConsoleColor.Blue);
                break;
        }
    }
    else
    {
        ColorConsole.log("Recertifying is bad", ConsoleColor.Red);
    }
}