﻿namespace Automatic_Campus_Net.Model;

public class ResultModel
{
    public int result { get; set; }

    public String msg { get; set; }

    public String ret_code { get; set; }
}