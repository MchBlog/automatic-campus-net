﻿using AutoInternet.Utils;

namespace Automatic_Campus_Net.Model;

public class RequestModel
{
    public String TargetIp { get; set; } = "182.61.200.6";

    public String Host { get; set; } = "http://10.8.200.95:801";

    [NeedToAddRequestResource] public String callback { get; set; } = "json";
    [NeedToAddRequestResource] public String login_method { get; set; } = "1";
    [NeedToAddRequestResource] public String user_account { get; set; } = String.Empty;
    [NeedToAddRequestResource] public String user_password { get; set; } = String.Empty;
    [NeedToAddRequestResource] public String wlan_user_ip { get; set; } = String.Empty;
    [NeedToAddRequestResource] public String? wlan_user_ipv6 { get; set; } = String.Empty;
    [NeedToAddRequestResource] public String wlan_user_mac { get; set; } = String.Empty;
    [NeedToAddRequestResource] public String? wlan_ac_name { get; set; } = String.Empty;
    [NeedToAddRequestResource] public String? wlan_ac_ip { get; set; } = String.Empty;
    [NeedToAddRequestResource] public String? jsVersion { get; set; } = String.Empty;
    [NeedToAddRequestResource] public String terminal_type { get; set; } = String.Empty;
    [NeedToAddRequestResource] public String lang { get; set; } = String.Empty;
    [NeedToAddRequestResource] public String v { get; set; } = String.Empty;

    public int MaxTimeOut { get; set; } = 3;
}