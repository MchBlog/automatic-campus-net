
# Automatic Campus Net
![license](https://img.shields.io/github/license/jerryc127/hexo-theme-butterfly?color=FF5531)

## 🚎介绍
<span STYLE="color: yellow">
注意：本软件只适用于河北科技工程职业技术大学
</span>

### ✨软件作用
自动连接西校区的校园网，东校区的未测试应该连不了

### 🎉如何使用

1. 连接学校的校园网（必须连接）
2. 打开本软件
3. 终端输出绿色的<span STYLE="color:green"> * </span> 表明校园网连接正常，每隔两秒输出一个
4. 当终端提示<span STYLE="color:red">Internet connection is bad</span>  表明校园网已断开，需重新连接
5. 当终端提示<span STYLE="color:blue">Reconnection succeeded</span>  表示校园网已经重新连接成功

### 🎇🎇🎆🎆最后预祝你网上冲浪愉快🎇🎇🎆🎆

## 💻 获取源代码
```bash
git clone https://gitee.com/MchBlog/automatic-campus-net.git
```
