
# Automatic Campus Net
![license](https://img.shields.io/github/license/jerryc127/hexo-theme-butterfly?color=FF5531)

![](/images/1.png)

## 🚎介绍
<span STYLE="color: yellow">
注意：本软件只适用于河北科技工程职业技术大学
</span>

### ✨软件作用
自动连接西校区的校园网，东校区的未测

### ⬇️如何下载

下载地址
[https://gitee.com/MchBlog/automatic-campus-net/releases](https://gitee.com/MchBlog/automatic-campus-net/releases)


**根据自己的电脑是否具备.NET6框架进行不同版本的下载**

### 🎉如何使用

1. 连接学校的校园网（必须连接）
2. 打开本软件，首次运行将自动生成配置文件`cconfig.json`
3. 打开浏览器进入校园网登录页面，点击F12，抓取信息并配置到`config.json`中
3. 终端输出绿色的<span STYLE="color:green"> * </span> 表明校园网连接正常，每隔两秒输出一个
4. 当终端提示<span STYLE="color:red">Internet connection is bad</span>  表明校园网已断开，需重新连接
5. 当终端提示<span STYLE="color:blue">Reconnection succeeded</span>  表示校园网已经重新连接成功


### ✨如何抓取数据

![](/images/2.png)

![](/images/3.png)
在请求URL中获取路由参数
### 🎇🎇🎆🎆最后预祝你网上冲浪愉快🎇🎇🎆🎆

## 💻 获取源代码
```bash
git clone https://gitee.com/MchBlog/automatic-campus-net.git
```



## ⚠️ 免责声明

**本项目仅供学习使用，违法违规使用与作者无关，均由使用者自行承担**
